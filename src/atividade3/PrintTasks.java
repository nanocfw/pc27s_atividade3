/**
 * Exemplo1: Programacao com threads
 * Autor: Marciano da Rocha
 * Ultima modificacao: 01/04/2018
 */
package atividade3;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PrintTasks implements Runnable
{

    enum CorSemaforo
    {
        VERDE, AMARELO, VERMELHO
    }

    private final int id;
    private final String semaforo; //nome do semáforo
    private final SimpleDateFormat sd;
    private CorSemaforo cor;

    public PrintTasks(String name, int id)
    {
        this.semaforo = name;
        this.id = id;
        sd = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss:SSS");

        cor = CorSemaforo.VERMELHO;
    }

    public void run()
    {
        while (true)
        {
            switch (cor)
            {
                case VERDE:
                {
                    cor = CorSemaforo.AMARELO;
                    System.out.printf(semaforo + " mudou para amarelo às " + sd.format(Calendar.getInstance().getTime()) + "\n");
                    break;
                }
                case AMARELO:
                {
                    cor = CorSemaforo.VERMELHO;
                    System.out.printf(semaforo + " mudou para vermelho às " + sd.format(Calendar.getInstance().getTime()) + "\n");
                    break;
                }
                case VERMELHO:
                {
                    cor = CorSemaforo.VERDE;
                    System.out.printf(semaforo + " mudou para verde às " + sd.format(Calendar.getInstance().getTime()) + "\n");
                    break;
                }
            }
            Thread.yield();
        }
    }

}
