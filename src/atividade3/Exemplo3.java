/**
 * Exemplo1: Programacao com threads
 * Autor: Marciano da Rocha
 * Ultima modificacao: 01/04/2018
 */
package atividade3;

public class Exemplo3
{

    public static void main(String[] args)
    {

        System.out.println("Inicio da criacao dos semáforos.");

        //Cria cada thread com um novo runnable selecionado
        Thread t;
        for (int i = 1; i <= 20; i++)
        {
            t = new Thread(new PrintTasks("Semáforo " + i, i));

            //Inicia as threads, e as coloca no estado EXECUTAVEL
            t.start(); //invoca o método run de t1
        }

        System.out.println("Semáforos criados");
    }

}
